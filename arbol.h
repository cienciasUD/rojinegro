#ifndef ARBOLBINARIO_H
#define ARBOLBINARIO_H


#include <iostream>

using namespace std;


/** Estructura nodo*/
struct NODO{ int info;
  NODO *izq, *der;
  int color;
  int hijo;
  NODO *padre;
};
/** La clase arbol, en la cual se realizara todo*/
class ARBOL{
  const int ROJO=1;
  const int NEGRO=0;
  const int DERECHO=1;
  const int IZQUIERDO=0;
  /** Apuntador a la raiz del arbol*/
  NODO *raiz;
  /** Contructor, por defecto no existira ningun raiz*/
  public:	ARBOL(){
    raiz=NULL;
  }

  NODO *raiz_arbol( ) {
    return raiz;
  }
  int insertar (int);
  NODO *buscar_arbol (int, NODO *, NODO *);
  void borrar_nodo(NODO *, NODO *);
  NODO *buscar(int, NODO **, NODO *);
  int retira_arbol(int);
  void inorden(NODO *);
  void preorden(NODO *);
  void posorden(NODO *);
  void destruir (NODO *p);
  void rotar(NODO *p);
  ~ARBOL( );
};
/*!
\brief "Se busca la posicion donde debe de ir un numero n"
\param n "El numero a ingresar"
\param *p El apuntador a la raiz_arbol
\param *q El apuntador al padre, usado en recursividad, dejar en NULL
\return "El padre de esa posicion"
*/NODO *ARBOL::buscar_arbol(int n, NODO *p, NODO *q){
if (p==NULL)
return q;
if (n < p->info){
  q=p;
  q=buscar_arbol(n,p->izq,q);
  return q;
}
else if (n > p->info) {
  q=p;
  q=buscar_arbol(n,p->der,q);
  return q;
}
else return NULL;
}
/*!
\brief "Se inserta un elemento al arbol"
\param n "El valor a insertar"
\pre "Pre-conditions"
\post "Post-conditions"
\return "Siempre se va a retornar un 0"
*/
int ARBOL:: insertar(int n){

  NODO *q;
  NODO *nuevo;
  NODO *padre;
  NODO *abuelo;
  NODO *tio;
  NODO *aux;

  /** Si no existe la raiz*/
  if (raiz == NULL){
    raiz= new NODO;
    raiz->info =n;
    raiz->izq=NULL;
    raiz->der=NULL;
    raiz->color=NEGRO;
    raiz->padre = NULL;
    raiz->hijo = -1;
    return 0;
  }
  q =buscar_arbol(n,raiz,NULL);
  if (q==NULL) return -1;

  /** Se llena el nuevo nodo*/
  nuevo= new NODO;
  nuevo->info = n;
  nuevo->izq=NULL;
  nuevo->der=NULL;
  nuevo->color=ROJO;
  nuevo->padre=q;



  /** Se agrega nuevo a la izquierda y se tiene el tio a la derecha*/
  if (n<q->info){
    q->izq=nuevo;
    nuevo->hijo=IZQUIERDO;
  }else if(n>q->info){
    q->der=nuevo;
    nuevo->hijo=DERECHO;
  }

  padre = nuevo->padre;
  abuelo = padre->padre;
  if (abuelo == NULL) {
    tio = NULL;
  }else{
    if(padre == (abuelo->izq)){
      tio = abuelo->der;
    }else{
      tio = abuelo->izq;
    }
  }

  if(nuevo -> color == ROJO && padre->color == ROJO){
    if(nuevo == padre->izq && tio == abuelo->izq){
      std::cout << "CASO 2" << '\n';
      //AQUI ROTAR SOBRE EL PADRE A LA IZQUIERDA
    }else if((nuevo == padre->der && tio == abuelo->der)){
      std::cout << "CASO 2 NUEVO Y TIO ESTAN A LA DERECHA" << '\n';
      //AQUI ROTAR SOBRE EL PADRE A LA DERECHA
    }

    if(tio->color == ROJO){
      std::cout << "CASO 1" << '\n';
      padre->color = NEGRO;
      tio ->color = NEGRO;
    }else{
      if(tio == abuelo->der){
        std::cout << "CASO 3" << '\n';
        padre -> color = NEGRO;
        abuelo -> color = ROJO;
        //AQUI ROTAR SOBRE ABUELO A LA DERECHA
      }else if(tio == abuelo -> izq){
        std::cout << "CASO 3" << '\n';
        padre -> color = NEGRO;
        abuelo -> color = ROJO;
        //AQUI ROTAR SOBRE ABUELO A LA IZQ
      }
    }
  }



  return 0;
}

void ARBOL::borrar_nodo (NODO *q, NODO *p){
  NODO  *r, *s,*t;
  if (p->izq==NULL)
  r=p->der;
  else if (p->der==NULL)
  r=p->izq;
  else {
    s =p;
    r=p->der;
    t=r->izq;
    while(t!=NULL){s=r; r=t; t=t->izq;
    }
    if (p!=s){
      s->izq=r->der;
      r->der=p->der;
    }
    r->izq =p->izq;
  }
  if (q==NULL) raiz= r;
  else if (p==q->izq) q->izq =r;
  else q->der = r;
  delete p;
}
NODO *ARBOL::buscar(int n, NODO **p, NODO *q){
  /** Apuntador a la raiz*/
  if (*p==NULL)
  return NULL;
  /** Se decide si bajar por izquierda o por derecha*/
  if (n <(*p)->info){
    q=*p;
    *p=(*p)->izq;
    q=buscar(n,p,q);
    return q;
  }
  else if (n>(*p)->info){
    q=*p;
    *p=(*p)->der;
    q =buscar(n,p,q);
    return q;
  }
  else return q;
}
int ARBOL:: retira_arbol(int n){

  /** La raiz del arbol*/
  NODO *p=raiz;
  /** El padre del nodo a eliminar*/
  NODO *q;
  q=buscar(n,&p,NULL);

  if (p==NULL) return -1;
  borrar_nodo(q,p);
  return 0;
}
void ARBOL::inorden(NODO *p){
  if (p!=NULL){
    inorden(p->izq);
    cout<<p->info<<endl;
    inorden(p->der);
  }
}
void ARBOL::preorden(NODO *p){
  if (p!=NULL){
    cout<<p->info<<endl;
    preorden(p->izq);
    preorden(p->der);
  }
}
void ARBOL::posorden(NODO *p){
  if (p!=NULL){
    posorden(p->izq);
    posorden(p->der);
    cout<<p->info<<endl;
  }
}
void ARBOL::destruir(NODO *p){
  if (p!=NULL){
    destruir(p->izq);
    destruir(p->der);
    delete p;
  }
}
ARBOL::~ARBOL( ) {destruir(raiz);
}

void ARBOL::rotar(NODO *p){
  

}
#endif
